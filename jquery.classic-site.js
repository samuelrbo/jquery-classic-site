/**
 * jQuery Classic Site
 *
 * Plugin to make the life easy to webmasters when client
 * need the classic view for the responsive site.
 *
 * @version 1.0
 * @since 2015-04-12 17:11
 * @license MIT
 * @author Samuel Ramon <samuelrbo@gmail.com>
 */
(function($){

	$.classicsite = function(options) {

		var settings = $.extend({
			cookiename: "jquery.classicsite",
			render_classic: true
		}, options);

		var cssData = new Array();

		if (cookieExists(settings.cookiename)) {
			removeStyles();
		}

		if (settings.render_classic && !cookieExists(settings.cookiename)) {

			$("link[data-responsive='true']").each(function(k, v){
				cssData.push(jQuery(v).attr("href"));
			});

			createCookie(settings.cookiename, JSON.stringify(cssData), 1);
			removeStyles();

		} else if (!settings.render_classic && cookieExists(settings.cookiename)) {

			var styles = $.parseJSON(readCookie(settings.cookiename));

			$.each(styles, function(i,o){
				$("head").append('<link rel="stylesheet" data-responsive="true" href="'+o+'" type="text/css" />')
			});

			eraseCookie(settings.cookiename)
		}

		this.showClassic = function() {
		    return !cookieExists(settings.cookiename)
		}

		return this;
	}

	function removeStyles () {
		$("link[data-responsive='true']").each(function(k, v){
			jQuery(v).remove();
		});
	}

	function cookieExists(name) {
		return readCookie(name) != null && readCookie(name) != "";
	}

	function createCookie(name, value, days) {
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            var expires = "; expires=" + date.toGMTString();
        }
        else var expires = "";

        document.cookie = name + "=" + value + expires + "; path=/";
    }

    function readCookie(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
        }
        return null;
    }

    function eraseCookie(name) {
        createCookie(name, "", -1);
    }

})(jQuery);
