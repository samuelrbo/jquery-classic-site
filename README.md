# jQuery Classic Site View #

A plugin to make the life easier for webmasters when their client needs the classic view for the responsive site.

Minified version created using [JSCompress Site](http://jscompress.com/)

## DONE ##

[2015-04-14]

* The plugin :D - Version 1.0

## Usage ##

### Setup ###

Call the plugin after the jQuery call

```html
<script src="path/to/jquery.classic-site.js"></script>
<!-- OR -->
<script src="path/to/jquery.classic-site.min.js"></script>
```

Add the attribute in the link tag of the stylesheets that contains the responsive style.

```html
<link rel="stylesheet" href="site.css" />
<link rel="stylesheet" href="responsive.css" data-responsive="true" />
```

### Calling ###

Call the function in the classic view button

```html
<a href="#" id="classic_btn">Show Classic View</a>
```

```js
(function(){
    jQuery("#classic_btn").click(function(e){
        e.preventDefault();

        jQuery.classicview({
            render_classic: jQuery.classicview.showClassic()
        });
    });
})();
```

## License ##

MIT License (see LICENSE file)

## Contributing ##

Your contribution is welcome. =D

## TODO ##

* Work with responsive frontend framewoks (Bootstrap, Foundation, Semantic, etc...)
* Create unity tests
* Provide an online example
